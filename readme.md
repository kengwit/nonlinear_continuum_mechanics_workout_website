# Nonlinear Continuum Mechanics: Tangent Moduli Derivations, Solutions and Recipes.
### By Keng-Wit Lim
=================

This is a short text on the derivation of tangent moduli for computational inelasticity at the finite strain. 

Originally, these notes were written in PDF (using LaTeX) during my PhD days at Caltech. I converted the PDF to a format I thought would be more appealing to people to access and read. You can preview and buy a Kindle version of the book here:

[https://www.amazon.com/dp/B07SH13VYK](https://www.amazon.com/dp/B07SH13VYK) 

Note: if you do not have a Kindle device, Kindle apps for PC and Mac are available for free on Amazon and these can be used to read the document.

Typos are listed in this [document](./typos/typos.pdf).

For general inquiries, please email: kengwit@gmail.com


![](./cover.png)

